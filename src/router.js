import Vue      from 'vue';
import Router   from 'vue-router';

import homePage          from './views/home.view.vue';
import productsPage      from './views/products.view.vue';
import productSearchPage from './views/product-search.view.vue';
import productDetailPage from './views/product-detail.view.vue';
import cartPage          from './views/cart.view.vue';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '*',
      redirect: '/',
    },
    {
      path: '/',
      name: 'homePage',
      component: homePage,
    },
    {
      path: '/products/:category',
      name: 'products',
      component: productsPage,
    },
    {
      path: '/search',
      name: 'search',
      component: productSearchPage,
    },
    {
      path: '/productDetail',
      name: 'productDetail',
      component: productDetailPage,
    },
    {
      path: '/cart',
      name: 'cart',
      component: cartPage,
    },
  ],
});

export default router;
