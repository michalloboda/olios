import Vue from 'vue';

Vue.filter('capitalize', word => word.toUpperCase());

Vue.filter('stringLimit', word => `${word.slice(0, 28)}...`);

Vue.filter('price', word => `$${word}`);
