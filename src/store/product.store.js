import axios from 'axios';

const state = {
  products: [],
  searchedProducts: [],
};

const getters = {
  searchedQuantity: state => state.searchedProducts.length,
};

const mutations = {
  setAllProducts(state, payload) {
    state.products = payload;
  },
  searchProducts(state, payload) {
    state.searchedProducts = [];
    if (payload === '') {
      return;
    }
    for (const product of state.products) {
      const regexp = new RegExp(payload, 'i');
      if (regexp.test(product.title) || regexp.test(product.description) || regexp.test(product.categories)) {
        state.searchedProducts.push(product);
      }
    }
  },
  resetSearchedResult(state) {
    state.searchedProducts = [];
  },
};

const actions = {
  async getAllProducts({ commit }) {
    await axios.get('https://api.myjson.com/bins/10eus5')
      .then(response => commit('setAllProducts', response.data.products));
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
