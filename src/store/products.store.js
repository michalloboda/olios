import axios from 'axios';

const state = {
  products: [],
  showedProducts: 6,
};

const mutations = {
  setProducts(state, payload) {
    state.products = payload;
  },
  showMoreProducts(state) {
    state.showedProducts += 6;
  },
  resetShowedProducts(state) {
    state.showedProducts = 6;
  },
};

const actions = {
  async getProducts({ commit, state }, category) {
    await axios.get('https://api.myjson.com/bins/10eus5')
      .then(response => commit('setProducts', response.data.products));
    if (category !== 'all') {
      const filteredProducts = state.products.filter(el => el.categories === category);
      commit('setProducts', filteredProducts);
    }
  },
};

export default {
  state,
  mutations,
  actions,
};
