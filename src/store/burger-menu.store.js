import axios from 'axios';

const state = {
  categories: [],
};

const mutations = {
  setCategories(state, payload) {
    state.categories = payload;
  },
};

const actions = {
  getCategories: ({ commit }) => {
    axios.get('https://api.myjson.com/bins/oqr65').then(response => commit('setCategories', response.data.categories));
  },
};

export default {
  state,
  mutations,
  actions,
};
