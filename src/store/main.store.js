import Vue           from 'vue';
import Vuex          from 'vuex';
import burgerMenu    from './burger-menu.store';
import productsPage  from './products.store';
import productSearch from './product.store';
import cartPage      from './cart.store';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    burgerMenu,
    productsPage,
    productSearch,
    cartPage,
  },
});
