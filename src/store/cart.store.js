const state = {
  cart: [],
};

const getters = {
  cartQuantity: state => state.cart.length,
  productQuantity: state => id => state.cart.filter(item => item.id === id).length,
};

const mutations = {
  addToCart(state, payload) {
    state.cart.push(payload);
  },
  removeFromCart(state, payload) {
    state.cart.splice(state.cart.lastIndexOf(payload), 1);
  },
  removeAllFromCart(state, payload) {
    state.cart = state.cart.filter(el => el.id !== payload.id);
  },
};

export default {
  state,
  getters,
  mutations,
};
